<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    protected $guarded = ['id'];
    protected $table = 'products';
    use HasFactory;

    public static function search($input)
    {
        $name =  isset($input['name']) ? $input['name'] : '';
        $description =  isset($input['description']) ? $input['description'] : null;
        $price =  isset($input['price']) ? $input['price'] : null;
        $price_min = (int) isset($input['price_min']) ? $input['price_min'] : null;
        $price_max = (int) isset($input['price_max']) ? $input['price_max'] : null;

        $query = self::orderByDesc('id');
        if (!empty($value = $name)) {

            $query->where('name', 'like', '%' . $value . '%');
        }
        if (!empty($value = $description)) {
            $query->where('description', 'like', '%' . $value . '%');
        }

        if(!empty($price_min) && !empty($price_max)){
            $query->whereBetween('products.price', [$price_min, $price_max]);
        }
        if(!empty($price_min) && empty($price_max)){
            $query->where('products.price', '>', $price_min);
        }
        if(empty($price_min) && !empty($price_max)){
            $query->where('products.price', '<', $price_max);
        }

        return $query->paginate(20);

    }


    public function imageUpload($file)
    {

        $path = public_path().'/images/';
        if(!file_exists($path) ){
            mkdir($path, 0777, true);
        }

        if($file->isValid()){
            $str = Str::random(8);
            $name = $str.'_' . $file->getClientOriginalName();

            $file->move($path, $name);
            $this->image = $name;
            $this->save();
        }
        return $this;
    }

}
