<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use Symfony\Component\HttpFoundation\Response;

class ProdoctController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->all();
        $posts = Product::search($input);
        return ProductResource::collection($posts);
    }

    public function store(ProductRequest $request)
    {
        $item = Product::create($request->all());

        $item->imageUpload($request->image);
        return response(new ProductResource($item), Response::HTTP_CREATED);
    }

    public function show($id)
    {
        return new ProductResource(Product::find($id));
    }

    public function update(ProductRequest $request, $id)
    {

        $item = Product::find($id);
        $input = $request->only('name', 'description', 'image', 'price');

        if(!$item){
            return response(['status' => 'ERROR', 'error' => 'Not found',
            ], Response::HTTP_NOT_FOUND);
        }
        $item->update($input);

        return response(new ProductResource($item), Response::HTTP_ACCEPTED);
    }

    public function destroy($id)
    {
        Product::destroy($id);

        return \response(null, Response::HTTP_NO_CONTENT);
    }
}
