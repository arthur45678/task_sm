<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'required|min:20|max:200',
            'price' => ['required', 'regex:/^(\d+(\.\d*)?)|(\.\d+)$/'],
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:4096',
        ];
    }
}
